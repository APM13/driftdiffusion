%u1=electrons, u2=holes, u3=I-, u4=potential
function [u1,u2,u3,u4,x,t] = drift1



% A routine to test solving the diffusion and drift equations using the
% matlab pde solver. Electrons as u1 and holes as u2,
% u3 is the electric potential.
%
% Piers Barnes last modified (3/07/2014)
% Andy McMahon last modified (29/4/2015)

% Parameters for spatial mesh of solution points
xmesh_type = 2;
xmax = 0.25;
x0 = xmax/10;
xpoints = 30;

% Parameters for time point outputs
tmesh_type = 2;
tmax = 0.4;
t0 = tmax/100;
%define ideal time step here ...
DeltaTime=1e-3;
tpoints = (tmax-t0)/DeltaTime;%100;

% intensity and absorption coefficient and illumination side
i0 = 0;%100;% incident photon flux

alpha = 1;% absorption coefficient
side = 1;% illumination side 1 = EE, 2 = SE

% electron mobility% hole mobility
%M.D.Hu J.Mat.Chem.A - 2014, effective masses
%me = 0.32
%mh = 0.37
%mue ~1/me so muh/mue ~ 0.87
% take upper limit from single crystals (Science paper)
mue = 1;
muh = 0.87;
%mobility of the iodine ions ....
% From C.Eames,Jarv,Piers et al. calculated
% diffusion constant is D~10^-12 cm^2/s.
% Einstein relation muI = D/kB*T
muI = 1e-10;

% Boltzmann constant
kB = 8.6173324e-5; %eV K^-1

% Temperature
T = 300; %K

% Dielectric constant
%from M.D.Hu above
%static ~ 5
%optical ~ 25
% Get these from Aurelien.
epp = 100;

% hole surface reaction rate
% these are rel. arbitrary (just make it big)...
% at open circuit 0
% short circuit are big.
% diff extraction rates in between these
% would give you a JV curve.
kexte = 100;%100;   %electron extraction rate at rhs
kexth = 1;%1;      %hole extraction rate at lhs
% units of m^3/s -> multiplies by two 
% conc's to give 1/m^3
% get this from Science paper as well (single crystal 
% parameters).
krec = 100;    %bimolecular recombination rate constant

% doping concentration and band bending
ND = 1;
VB = 0.5;

% Define geometry of the system (m=0 1D, m=1 cylindrical polar coordinates,
% m=2 spherical polar coordinates).
m = 0;

% define solution mesh either logarithmically or linearly spaced points
if xmesh_type == 1
    x = linspace(x0,xmax,xpoints);
elseif xmesh_type == 2
    x = logspace(log10(x0),log10(xmax),xpoints);
end

if tmesh_type == 1
    t = linspace(t0,tmax,tpoints);
elseif tmesh_type == 2
    t = logspace(log10(t0),log10(tmax),tpoints);
end

% Tweak solver options - limit maximum time step size during integration.
options = odeset('MaxStep',t0/10);

% Call solver - inputs with '@' are function handles to the subfunctions
% below for the: equation, initial conditions, boundary conditions
sol = pdepe(m,@pdex4pde,@pdex4ic,@pdex4bc,x,t,options);

% split the solution into its component parts (e.g. electrons, holes and
% potential)
u1 = sol(:,:,1);
u2 = sol(:,:,2);
u3 = sol(:,:,3);
u4 = sol(:,:,4);

% plot the output
figure(1);
surf(x,t,u1);
title('n(x,t)');
xlabel('Distance x');
ylabel('time');

figure(2);
surf(x,t,u2);
title('p(x,t)');
xlabel('Distance x');
ylabel('time');

figure(3);
surf(x,t,u3);
title('Iodine(x,t)');
xlabel('Distance x');
ylabel('time');

figure(4);
surf(x,t,u4);
title('V(x,t)');
xlabel('Distance x');
ylabel('time');

% find the internal current density in the device
% Jndiff = [];
% Jnddrift_ = [];
% Jpdiff = [];
% Jpdrift= [];
% JIdiff = [];
% JIdrift = [];
% J = [];
Jndiff = zeros(length(x),length(t));
Jnddrift_ = zeros(length(x),length(t));
Jpdiff = zeros(length(x),length(t));
Jpdrift= zeros(length(x),length(t));
JIdiff = zeros(length(x),length(t));
JIdrift = zeros(length(x),length(t));
J = zeros(length(x),length(t));

%loop over time and store j(x) for each t to get j(x,t)
for j=1:length(t)
 %time = t(j);
% Evaluate the contributions to flux of charges
for i=1:length(x)
    xlocal = x(i);
    [nlocal,dnlocaldx] = pdeval(0,x,u1(j,:),xlocal);
    [plocal,dplocaldx] = pdeval(0,x,u2(j,:),xlocal);
    [Ilocal,dIlocaldx] = pdeval(0,x,u3(j,:),xlocal);
    [Vlocal,dVlocaldx] = pdeval(0,x,u4(j,:),xlocal);
    Jndiff(i,j) = mue*kB*T*dnlocaldx;
    Jndrift(i,j) = -mue*nlocal*dVlocaldx;
    Jpdiff(i,j) = -muh*kB*T*dplocaldx;
    Jpdrift(i,j) = -muh*plocal*dVlocaldx;
    JIdiff(i,j) = muI*kB*T*dIlocaldx;
    JIdrift(i,j) = -muI*Ilocal*dVlocaldx;
    J(i,j) = Jndiff(i,j) + Jndrift(i,j) + Jpdiff(i,j) + Jpdrift(i,j) + JIdiff(i,j) + JIdrift(i,j);
end
end

figure(5);
surf(x,t,transpose(J));
xlabel('x');
ylabel('t');
title('Total Current Density');

figure(6) 
for k=1:40:length(t)
    plot(J(:,k))
    hold on
end
Legend=cell(length(t),1)
 for iter=1:length(t)
   Legend{iter}=strcat('t = ', num2str(iter));
 end
 legend(Legend)
hold off

% figure(6)
% plot(x,Jndiff,'m',x,Jndrift,'b',x,Jpdiff,'r',x,Jpdrift,'g',x,J,'k')
% legend('Jn diff','Jn drift','Jp diff','Jp drift','Total J')
% xlabel('x [cm]')
% ylabel('Jn, Jp, J [A cm^-2]')
% title('Current densities')
% hold off


% --------------------------------------------------------------------------
% Set up partial differential equation (see MATLAB pdepe help for details of c, f
% and s)
function [c,f,s,iterations] = pdex4pde(x,t,u,DuDx)

% Prefactors set to 1 for time dependent components - can add other
% functions if you want to include the multiple trapping model
c = [1
     1
     1
     0]; %this was 0
 
% Flux terms, in this example I've given different mobilities to the
% electrons and holes.  Obviously you will need to put numerically correct
% relationships in.
f = [mue*(-u(1)*DuDx(4)+kB*T*DuDx(1))
     muh*(u(2)*DuDx(4)+kB*T*DuDx(2))
     muI*(-u(3)*DuDx(4)+kB*T*DuDx(3))
     DuDx(4)];   

 % Source/sink terms to define the generation (10) and recombinatoin rates for
 % electrons and holes.  The potential is also calculated, you will note
 % that I have included an intrinsic dopoing concentration of '1' for the
 % variable u(1).
 
 if side == 1
     g = i0*alpha*exp(-alpha*(x-x0));
 elseif side == 2
     g = i0*alpha*exp(-alpha*(xmax-(x-x0)));
 else
     g = 10;
 end 
 s = [g-krec*u(1)*u(2)
      g-krec*u(1)*u(2)
      0
      epp*(-u(1)+ND+u(2)-u(3))];
  
end

% --------------------------------------------------------------------------

% Define initial conditions.  In this case there is no field initially and
% the doping concentration of u1 is 1 at all points.
function u0 = pdex4ic(x)
u0 = [ND
      0
      0.04
      0];
  
end

% --------------------------------------------------------------------------

% Define boundary condtions, refer pdepe help for the precise meaning of p
% and you l and r refer to left and right.
% in this example I am controlling the flux through the boundaries using
% the difference in concentration from equilibrium and the extraction
% coefficient.
function [pl,ql,pr,qr] = pdex4bc(xl,ul,xr,ur,t)

pl = [0; -kexth*ul(2); 1; ul(4)-0];
ql = [1; 1; 1; 0];
pr = [kexte*(ur(1)-ND); 1; 0; ur(4)-VB];
qr = [1; 1; 1; 0];
                                  
end

end
